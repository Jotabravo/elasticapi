<?php
namespace Elastic\Api;

class Client
{
	private $server_ip;

	private $server_port;

	private $server_url;

	private $server_user;

	private $server_password;

	private $index;

	private $config;

	private $action_url;

	private $action_url_suffix;

	private $aggregation_name;

	public function __construct($connection, $index)
	{
		$this->server_ip = $connection['ip'];
		$this->server_port = $connection['port'];
		$this->server_url = 'http://' . $this->server_ip . ':' . $this->server_port;
		$this->server_user = $connection['user'];
		$this->server_password = $connection['password'];
		$this->index = $index;
		$this->config = null;
		$this->aggregation_name = null;
	}

	public function getServerIp()
	{
		return $this->server_ip;
	}

	public function setServerIp($server_ip)
	{
		$this->server_ip = $server_ip;
	}

	public function getServerPort()
	{
		return $this->server_port;
	}

	public function setServerPort($server_port)
	{
		$this->server_port = $server_port;
	}

	public function getServerUrl()
	{
		return $this->server_url;
	}

	public function getServerUser()
	{
		return $this->server_user;
	}

	public function setServerUser($server_user)
	{
		$this->server_user = $server_user;
	}

	public function getServerPassword()
	{
		return $this->server_password;
	}

	public function setServerPassword($server_password)
	{
		$this->server_password = $server_password;
	}

	public function getIndex()
	{
		return $this->index;
	}

	public function setIndex($index)
	{
		$this->index = $index;
	}

	public function getConfig()
	{
		return $this->config;
	}

	public function setConfig($config)
	{
		$this->config = $config;
	}

	public function getActionUrl()
	{
		return $this->action_url;
	}

	public function setActionUrl()
	{
		$this->action_url = $this->server_url . '/' . $this->index . '/' . $this->action_url_suffix;
	}

	public function setMlActionUrl()
	{
		$this->action_url = $this->server_url . '/_xpack/ml/anomaly_detectors/' . $this->index . '/results/' . $this->action_url_suffix;
	}

	public function getActionUrlSuffix()
	{
		return $this->action_url_suffix;
	}

	public function setActionUrlSuffix($action_url_suffix)
	{
		$this->action_url_suffix = $action_url_suffix;
	}

	public function getAggregationName()
	{
		return $this->aggregation_name;
	}

	public function setAggregationName($aggregation_name)
	{
		$this->aggregation_name = $aggregation_name;
	}

    public function handleCurlRequest()
    {
    	try {
	    	ini_set('memory_limit', '8096M'); // Se coloca esto si aparece el error de Exhausted Memory en PHP

	        $hasConfig = isset($this->config) && !empty($this->config);
	        $hasPostFields = $hasConfig && isset($this->config['postFields']);
	        $hasHeaders = $hasConfig && isset($this->config['headers']);
	        $hasMethod = $hasConfig && isset($this->config['method']);
	        $hasOutput = $hasConfig && isset($this->config['output']);
	        $ch = curl_init();

	        curl_setopt($ch, CURLOPT_URL, $this->action_url);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, $hasOutput ? 1 : 0);

	        if ($hasPostFields)
	            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($this->config['postFields']));

	        if ($hasMethod)
	            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($this->config['method']));
	        else
	            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

	        curl_setopt($ch, CURLOPT_USERPWD, "{$this->server_user}:{$this->server_password}");

	        if ($hasHeaders) {
	            if (is_string($this->config['headers']) && $this->config['headers'] === 'json') {
	                $headers = array("Content-Type: application/json");
	                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	            } elseif (is_array($this->config['headers'])) {
	                curl_setopt($ch, CURLOPT_HTTPHEADER, $this->config['headers']);
	            }
	        }

	        $result = curl_exec($ch);
	        if (curl_errno($ch)) {
	            throw new \Exception(curl_error($ch));
	        }

	        curl_close($ch);

	        if ($hasOutput) {
	            if ($this->config['output'] === 'object')
	                return json_decode($result);
	            if ($this->config['output'] === 'array')
	                return json_decode($result, true);
	            else
	                return $result;
	        }
    	} catch (\Exception $e) {
    		throw new \Exception($e->getMessage());
    	}
    }

    public function handleData()
    {
    	$this->setActionUrl();
    	$data = $this->handleCurlRequest();

    	if ($this->aggregation_name) {
    		$hits = isset($data["aggregations"][$this->aggregation_name]["buckets"]) ? $data["aggregations"][$this->aggregation_name]["buckets"] : array();
    	} else {
    		$hits = isset($data["hits"]["hits"]) ? $data["hits"]["hits"] : array();
    	}

	    $total_hits = count($hits);
	    $response = array('total' => $total_hits, 'data' => $hits);

	    return $response;
    }

    public function handleMlData()
    {
    	$this->setMlActionUrl();
    	$data = $this->handleCurlRequest();

    	$hits = isset($data["records"]) ? $data["records"] : array();
	    $total_hits = count($hits);
	    $response = array('total' => $total_hits, 'data' => $hits);

	    return $response;
    }
}
